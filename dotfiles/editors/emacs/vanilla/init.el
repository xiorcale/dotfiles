;;; init.el --- kjuvi's own emacs configuration
;;;
;;; Commentary:
;;; This entire Emacs is self contained inside the emacs.nix definition
;;; which is in the same folder as this file. The only requirement to install
;;; it is to have the Nix package manager and run `'nix-env -if emacs.nix`'.
;;;
;;; Code:

;; speed-up init time and lsp-mode
(setq gc-cons-threshold 100000000
      read-process-output-max (* 1024 1024))
(let ((file-name-handler-alist nil))

  (add-to-list 'load-path "~/.emacs.d/lisp/")

  (require 'package)
  (add-to-list 'package-archives (cons "melpa" "https://melpa.org/packages/") t)

  (package-initialize)

  (load "config.el")
  (load "functions.el")
  ;(load "org-config.el")
  (load "packages.el"))

;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(initial-frame-alist (quote ((fullscreen . maximized))))
 '(package-selected-packages
   (quote
    (which-key projectile lsp-treemacs lsp-ivy company-lsp lsp-ui lsp-mode ivy flycheck doom-modeline dockerfile-mode company command-mode all-the-icons use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
