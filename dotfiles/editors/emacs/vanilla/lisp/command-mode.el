;;; command-mode.el --- brings light modal editing to emacs, without loosing the vanilla experience
;;;
;;; Commentary:
;;; This mode allows to use common Emacs keybinding to move cursor (C-p, C-n, ...) without
;;; the Ctrl key. Prefixing the command with a number N will repeat it N times.
;;;
;;; Code:

(defvar command/alter-cursor nil
  "Alter the cursor style when `command-mode' is active?")

(defvar command/cursor-type-active 'hollow
  "The cursor style to be used when `command-mode' is active (if \
`command/alter-cursor' is non-nil).")

(defvar command/cursor-type-inactive cursor-type
  "The cursor style to be used when `comand-mode' is not active (if \
`command/alter-cursor' is non-nil).")

(defun command/change-cursor-type (enable)
  "If `command/alter-cursor' and ENABLE are both non-nil, changethe \
style to `command/cursor-type-active'."
  (interactive)
  (when command/alter-cursor
    (setq cursor-type (if (and command/alter-cursor enable)
                          command/cursor-type-active
                        ;; else
                        command/cursor-type-inactive))))

;; (defun command/set-visual-indicators (enable)
;;   "If ENABLE, set visuals indicators when `command-mode' is active."
;;   (interactive)
;;   (command/change-cursor-type enable))

(defun exit-command-mode ()
  "Exit `command-mode'."
  (interactive)
  (command/change-cursor-type nil)
  (command-mode -1))

(defun enter-command-mode ()
  "Enter `command-mode'."
  (interactive)
  (command-mode 1)
  (command/change-cursor-type t))

(defun toggle-command-mode ()
  "Turn `command-mode' on or off."
  (interactive)
  (if command-mode
      (exit-command-mode)
    ;; else
    (enter-command-mode)))

(define-minor-mode command-mode
  "Toggle command-mode, a modal editing for Emacs."
  :init-value nil
  :lighter " cmd"
  :keymap (let ((map (make-sparse-keymap)))

            (global-set-key (kbd "<escape>") 'enter-command-mode)
            (define-key map (kbd "i") 'exit-command-mode)

            ; moving around
            (define-key map (kbd "n") 'next-line)
            (define-key map (kbd "p") 'previous-line)
            (define-key map (kbd "f") 'forward-char)
            (define-key map (kbd "b") 'backward-char)

            (define-key map (kbd "a") 'beginning-of-line-text)
            (define-key map (kbd "e") 'end-of-line)

            (define-key map (kbd "<") 'beginning-of-buffer)
            (define-key map (kbd ">") 'end-of-buffer)

            ; deleting
            (define-key map (kbd "d") 'delete-char)
            (define-key map (kbd "k") 'kill-line)

            ; cut/yank
            (define-key map (kbd "w") 'clipboard-kill-ring-save)
            (define-key map (kbd "y") 'yank)

            ; files/directories
            (define-key map (kbd "xf") 'find-file)
            (define-key map (kbd "xd") 'dired)

            ; save, undo
            (define-key map (kbd "xs") 'save-buffer)
            (define-key map (kbd "xu") 'undo)

            ; buffers
            (define-key map (kbd "xb") 'switch-to-buffer)
            (define-key map (kbd "xk") 'kill-buffer)

            ; windows
            (define-key map (kbd "xo") 'other-window)
            (define-key map (kbd "x0") 'delete-window)
            (define-key map (kbd "x1") 'delete-other-windows)

            ; quit
            (define-key map (kbd "xc") 'save-buffers-kill-terminal)

            ; arguments
            (define-key map (kbd "1") [?\C-1])
            (define-key map (kbd "2") [?\C-2])
            (define-key map (kbd "3") [?\C-3])
            (define-key map (kbd "4") [?\C-4])
            (define-key map (kbd "5") [?\C-5])
            (define-key map (kbd "6") [?\C-6])
            (define-key map (kbd "7") [?\C-7])
            (define-key map (kbd "8") [?\C-8])
            (define-key map (kbd "9") [?\C-9])
            (define-key map (kbd "0") [?\C-0])
            
            map))

(provide 'command-mode)

;;; command-mode.el ends here

