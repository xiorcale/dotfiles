;;; org-config.el --- org mode configuration
;;;
;;; Commentary:
;;; Org mode configuration for latex export, agenda, etc...
;;;
;;; Code:

(use-package org
  :config
  (setq org-agenda-files '("~/Documents/org"))
  (setq org-capture-templates
        '(
          ("t" "Todo"
           entry (file+datetree "~/Documents/org/todo.org")
           "* %?"
           :empty-lines 1)
          ("j" "Journal Entry"
           entry (file+datetree "~/Documents/org/journal.org")
           "* %?"
           :empty-lines 1)
          ))

  ;; latex export config
  (add-to-list 'org-latex-packages-alist
               '("AUTO" "babel" t ("pdflatex")))

  (setq org-latex-listings t)
  :bind
  ("C-c c" . 'org-capture)
  ("C-c a" . 'org-agenda)
  )

;;; org-config.el ends here

