;;; functions.el --- functions definitions
;;;
;;; Commentary:
;;; Various utility functions.
;;;
;;; Code:

(defun find-file-as-root (file-path)
    "Open a file at FILE-PATH as administrator."
    (interactive "sFind file: ")
    (find-file (concat "/sudo::" file-path)))

(defun edit-buffer-as-root ()
  "Edit buffer file as root."
  (interactive)
  (if buffer-file-name
      (find-alternate-file (concat "/sudo::" buffer-file-name))))

(defun split-and-follow-horizontally ()
    "Split window below and grab the focus immediatly."
    (interactive)
    (split-window-below)
    (other-window 1))
(global-set-key (kbd "C-x 2") 'split-and-follow-horizontally)

(defun split-and-follow-vertically ()
    "Split window right and grab the focus immediatly."
    (interactive)
    (split-window-right)
    (other-window 1))
(global-set-key (kbd "C-x 3") 'split-and-follow-vertically)

;;; functions.el ends here
