#!/bin/bash

current_date=$(date "+%Y-%m-%d %H:%M %p")

SINK=0
volume_status=$(pactl list sinks | grep Mute | head -n $(( $SINK + 1 )) | tail -n 1 | cut -c 8-)

if [ $volume_status == "yes" ]; then
    volume_level="[m]"
else
    volume_level=$(pactl list sinks | grep '^[[:space:]]Volume:' | head -n $(( $SINK + 1 )) | tail -n 1 | sed -e 's,.* \([0-9][0-9]*\)%.*,\1,')%
fi

echo "$volume_level | $current_date"
