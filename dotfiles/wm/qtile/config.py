import os
import subprocess

from libqtile.config import Key, Screen, Group, Drag, Click
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook

from backlight import increment, decrement

try:
    from typing import List  # noqa: F401
except ImportError:
    pass


@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])

mod = "mod4"

keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down()),
    Key([mod, "control"], "j", lazy.layout.shuffle_up()),

    # resize split layout
    Key([mod], "i", lazy.layout.grow()),
    Key([mod], "m", lazy.layout.shrink()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    Key([mod], "Return", lazy.spawn("kitty")),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod], "w", lazy.window.kill()),

    Key(["control", "mod1"], "l", lazy.spawn("dm-tool lock")),
    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),

    Key([mod], "r", lazy.spawncmd()),
    Key([mod], "d", lazy.spawn("rofi -show drun")),
    Key([mod, "shift"], "d", lazy.spawn("sh -c ~/.config/rofi/scripts/power.sh")),

    Key([mod], "p", lazy.spawn("sh -c ~/.config/rofi/scripts/monitor_layout.sh")),

    # Media keys
    Key([], "XF86AudioMute", lazy.spawn("pactl set-sink-mute 0 toggle")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("pactl set-sink-volume 0 -5%")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("pactl set-sink-volume 0 +5%")),
    Key([], "XF86MonBrightnessDown", decrement(10)),
    Key([], "XF86MonBrightnessUp", increment(10)),
]

groups = [
    Group(u'\ue927'),
    Group(u'\ue877'),
    Group(u'\ue915'),
    Group(u'\ue871'),
    Group(u'\ue90a'),
]

i = 1
for group in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], str(i), lazy.group[group.name].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], str(i), lazy.window.togroup(group.name)),
    ])
    i += 1

layouts = [
    layout.MonadTall(
        border_focus='268bd2',
        # border_normal='1d1f21',
        border_width=1,
        margin=15,
    ),
    layout.Max(),
]

widget_defaults = dict(
    font='DejaVu Sans',
    fontsize=14,
    padding=4,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(
                    active='c5c8c6',
                    highlight_method='line',
                    highlight_color=['1d1f21', '1d1f21'],
                    this_current_screen_border='#268bd2',
                    urgent_border='dc322f',
                    borderwidth=3,
                    disable_drag=True,
                    center_aligned=True,
                ),
                widget.Spacer(length=750),
                widget.TextBox(u'\ue84d', font='feather', fontsize=16),
                widget.Clock(format='%I:%M %p'),
                widget.Spacer(length=18),
                widget.Prompt(),
                widget.Spacer(),
                widget.TextBox(u'\ue8ef', font='feather', fontsize=16),
                widget.Volume(),
                widget.Spacer(length=18),
                widget.TextBox(u'\ue832', font='feather', fontsize=16),
                widget.Battery(format='{percent:2.0%}'),
                widget.Systray(padding=18, icon_size=24),
                widget.Spacer(length=18),
            ],
            30,
            background='#1d1f21',
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(
    float_rules=[
        {'wmclass': 'confirm'},
        {'wmclass': 'dialog'},
        {'wmclass': 'download'},
        {'wmclass': 'error'},
        {'wmclass': 'file_progress'},
        {'wmclass': 'notification'},
        {'wmclass': 'splash'},
        {'wmclass': 'toolbar'},
        {'wmclass': 'confirmreset'},  # gitk
        {'wmclass': 'makebranch'},  # gitk
        {'wmclass': 'maketag'},  # gitk
        {'wname': 'branchdialog'},  # gitk
        {'wname': 'pinentry'},  # GPG key password entry
        {'wmclass': 'ssh-askpass'},  # ssh-askpass
    ],
    border_focus='268bd2',
)
auto_fullscreen = True
focus_on_window_activation = "smart"

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, github issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
