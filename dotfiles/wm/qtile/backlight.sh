#!/bin/sh

max_brightness=$( cat /sys/class/backlight/intel_backlight/max_brightness )
brightness=$( cat /sys/class/backlight/intel_backlight/brightness )


increment() {
    if [ "$brightness" -lt "$max_brightness" ]
    then
        brightness=$((brightness+$1))
        echo $brightness > /sys/class/backlight/intel_backlight/brightness
    fi
}

decrement() {
    if [ "$brightness" -gt 100 ]
    then
        brightness=$((brightness-$1))
        echo $brightness > /sys/class/backlight/intel_backlight/brightness
    fi
}

help() {
    echo "Usage: backlight [OPTION] [STEP]"
    echo "Increase or decrease the backlight by STEP"
    echo
    echo "  -i [STEP]                 increment backlight by STEP"
    echo "  -d [STEP]                 decrement backlight by STEP\n"
}



if [ $# -eq 2 ]
then
    STEP=50
    if [ "$1" == "-i" ]
    then
        increment $2
    elif [ "$1" == "-d" ]
    then
        decrement $2
    else
        help
    fi
else
    help
fi

