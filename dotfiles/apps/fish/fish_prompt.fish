set -l nix_shell_info (
  if test "$IN_NIX_SHELL" = "1"
    echo -n -s "$nix_shell_info ~>"
  end
)
