{ pkgs, ... }:

with pkgs;

{
  home-manager.users.kjuvi = {
    programs.neovim = {
      enable = true;
      withPython3 = true;
      withNodeJs = true;
      vimAlias = true;
    };
  };

  environment.systemPackages = [
    ripgrep

    python37Packages.autopep8
    python37Packages.jedi
    python37Packages.pylint
    python37Packages.python-language-server

    nodejs
    nodePackages.typescript
    nodePackages.typescript-language-server
  ];
}
